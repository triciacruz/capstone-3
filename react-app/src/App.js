//Importables
import './App.css';
import {UserProvider} from './UserContext.js'

//React Importables 
import {Container} from 'react-bootstrap';
import {Fragment, useState} from 'react'
import {BrowserRouter as Router} from 'react-router-dom'
import {Route, Routes} from 'react-router-dom'


//Importables from components
import AppNavbar from './components/AppNavbar.js';
import View from './components/View.js';
import DashboardTable from './components/DashboardTable.js';
import ViewAdd from './components/ViewAdd.js';
import ViewUpdate from './components/ViewUpdate.js';
import ViewArchive from './components/ViewArchive.js';
import ViewActivate from './components/ViewActivate.js';

//Importables from pages
import Home from './pages/Home.js'
import Register from './pages/Register.js'
import Login from './pages/Login.js'
import Logout from './pages/Logout.js'
import NotFound from './pages/NotFound.js'
import Products from './pages/Products.js'
import Dashboard from './pages/Dashboard.js'
import About from './pages/About.js'


//Component Function
function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  return (
    <UserProvider value={{user, setUser, unsetUser}}>

      <Router>
       <AppNavbar/>
        <Container fluid>
          <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/about" element={<About/>}/>
            <Route path="/register" element={<Register/>}/>
            <Route path="/login" element={<Login/>}/>
            <Route path="/logout" element={<Logout/>}/>
            <Route path="*" element={<NotFound/>}/>
            <Route path="/products" element={<Products/>}/>
            <Route path="/products/active/:productId/view" element={<View/>}/> 
            <Route path="/dashboard" element={<Dashboard/>}/>
            <Route path="/products/add/view" element={<ViewAdd/>}/> 
            <Route path="/products/:productId/update/view" element={<ViewUpdate/>}/>
            <Route path="/products/:productId/archive/view" element={<ViewArchive/>}/>
            <Route path="/products/:productId/activate/view" element={<ViewActivate/>}/>
          </Routes>
        </Container>
      </Router>

    </UserProvider>
  );
}

//Exporting of the component Function
export default App; 
