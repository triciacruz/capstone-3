import React, { useContext } from 'react';
import { Container, Navbar, Nav, NavDropdown } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar() {
  const { user } = useContext(UserContext);

  return (
    <Navbar className="navbar-bg" expand="lg">
      <Container>
        <Navbar.Brand as={Link} to="/" className="mr-auto px-5 mr-3" xs={12} md={4} style={{ fontSize: '1.6rem' }}>
          The Bookish Hub
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto px-5" xs={12} md={4}>
            <Nav.Link as={NavLink} to="/" className="p-3" style={{ fontSize: '1.1rem' }}>
              Home
            </Nav.Link>
            {user.isAdmin ? (
              <Nav.Link as={NavLink} to="/dashboard" className="p-3" style={{ fontSize: '1.1rem' }}>
                Dashboard
              </Nav.Link>
            ) : (
              <Nav.Link as={NavLink} to="/products" className="p-3" style={{ fontSize: '1.1rem' }}>
                Catalog
              </Nav.Link>
            )}

              <Nav.Link as={NavLink} to="/about" className="p-3" style={{ fontSize: '1.1rem' }}>
                About Us
              </Nav.Link>


            {user.id !== null ? (
              <Nav.Link as={NavLink} to="/logout" className="p-3" style={{ fontSize: '1.1rem' }}>
                Logout
              </Nav.Link>
            ) : (
              <NavDropdown
                title=""
                id="collasible-nav-dropdown"
                className="p-2 text-nowrap"
                style={{ maxWidth: '150px', fontSize: '1.1rem' }}
              >
                <Nav.Link as={NavLink} to="/login">
                  Login
                </Nav.Link>
                <Nav.Link as={NavLink} to="/register">
                  Register
                </Nav.Link>
              </NavDropdown>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}