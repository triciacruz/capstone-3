import { Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({isAdmin}) {
  return (
    <Row>
      <Col className="text-center pt-4">
        <h1  style={{color: '#DCD9B9', fontSize: '4rem' }}>The Bookish Hub</h1>
        <p  style={{ fontSize: '1.4rem' }}>Building Bridges Between Books and Readers</p>
      
      </Col>
    </Row>
  );
}
