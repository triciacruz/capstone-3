import { Table, Row, Col, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link, useNavigate } from 'react-router-dom';

export default function DashboardTable({ allProducts }) {
  const navigate = useNavigate();

  const handleAddProduct = () => {
    navigate('/products/add/view');
  };

  const handleUpdateProduct = (productId) => {
    navigate(`/products/${productId}/update/view`);
  };

  const handleArchiveProduct = (productId) => {
    navigate(`/products/${productId}/archive/view`);
  };

  const handleActivateProduct = (productId) => {
    navigate(`/products/${productId}/activate/view`);
  };

  return (
    <>
      <Row className="p-5">
        <Col xs={6} className="text-start">
          <h1>Admin Dashboard</h1>
        </Col>
        <Col xs={6} className="text-end">
          <Button variant="none" style={{ color: 'black', backgroundColor: '#DCD9B9' }} onClick={handleAddProduct}>
            + Add Product
          </Button>
        </Col>
      </Row>

      <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
        <Table bordered hover style={{ width: '90%' }} size="lg">
          <thead>
            <tr>
              <th style={{ verticalAlign: 'middle', textAlign: 'center' }}>ID</th>
              <th style={{ verticalAlign: 'middle', textAlign: 'center' }}>NAME</th>
              <th style={{ verticalAlign: 'middle', textAlign: 'center', width: '25%' }}>DESCRIPTION</th>
              <th style={{ verticalAlign: 'middle', textAlign: 'center' }}>PRICE</th>
              <th style={{ verticalAlign: 'middle', textAlign: 'center' }}>AVAILABILITY</th>
              <th style={{ verticalAlign: 'middle', textAlign: 'center' }} colSpan={2}>
                ACTIONS
              </th>
            </tr>
          </thead>

          <tbody>
            {allProducts.map((product) => (
              <tr key={product._id}>
                <td>{product._id}</td>
                <td>{product.name}</td>
                <td style={{ maxWidth: '25%' }}>{product.description}</td>
                <td>₱ {product.price}</td>
                <td>{product.isActive ? 'IN STOCK' : 'OUT OF STOCK'}</td>
                <td>
                  <Link to={`/products/${product._id}/update/view`}>
                    <Button
                      variant="none"
                      style={{ color: 'black', backgroundColor: '#DCD9B9' }}
                      onClick={() => handleUpdateProduct(product._id)}
                    >
                      Edit
                    </Button>
                  </Link>
                </td>
                <td>
                  {product.isActive ? (
                    <Link to={`/products/${product._id}/archive/view`}>
                      <Button
                        variant="none"
                        style={{ color: 'black', backgroundColor: '#DCD9B9' }}
                        onClick={() => handleArchiveProduct(product._id)}
                      >
                        Disable
                      </Button>
                    </Link>
                  ) : (
                    <Link to={`/products/${product._id}/activate/view`}>
                      <Button
                        variant="none"
                        style={{ color: 'black', backgroundColor: '#DCD9B9' }}
                        onClick={() => handleActivateProduct(product._id)}
                      >
                        Enable
                      </Button>
                    </Link>
                  )}
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
    </>
  );
}

DashboardTable.propTypes = {
  allProducts: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
      isActive: PropTypes.bool.isRequired,
    })
  ).isRequired,
};
