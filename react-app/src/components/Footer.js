import { Container } from 'react-bootstrap';

export default function Footer() {
  return (
    <div className="text-center bg-dark text-white">
      <p>&copy; 2023 The Bookish Hub. All rights reserved.</p>
    </div>
  );
}
