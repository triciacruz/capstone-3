import React, { useContext } from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import bookishImage from '../img/bookish.png';

export default function Highlights() {
  const { user } = useContext(UserContext);

  return (
    <Row className="justify-content-center pt-2">
      <Col xs={12} md={4} className="w-100">
        <Card className="cardHighlight mx-auto">
          <div className="cardOverlay">
            <Card.Img
              variant="top"
              src={bookishImage}
              className="stretch-image remove-horizontal-padding"
              alt="Bookish"
            />
            <div className="cardText">
              <Card.Title style={{color: '#3c3e3c', fontSize: '3rem' }}>All Books</Card.Title>
              <Card.Text style={{color: '#3c3e3c', fontSize: '1.2rem' }}>See all available books</Card.Text>
              {user.isAdmin ? (
                <Link to="/dashboard">
                  <Button
                    variant="none"
                    style={{ color: 'black', backgroundColor: '#DCD9B9', fontSize: '1.2rem' }}
                  >
                    Go to Dashboard
                  </Button>
                </Link>
              ) : (
                <Link to="/products">
                  <Button
                    variant="none"
                    style={{ color: 'black', backgroundColor: '#DCD9B9', fontSize: '1.2rem' }}
                  >
                    Explore All Books
                  </Button>
                </Link>
              )}
            </div>
          </div>
        </Card>
      </Col>
    </Row>
  );
}
