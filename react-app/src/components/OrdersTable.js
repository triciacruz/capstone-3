import { Table, Row, Col, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

export default function OrdersTable({ allOrders }) {
  const handleAddProduct = () => {
    // Placeholder function for adding a product
    console.log('Add Product clicked');
  };

  if (!Array.isArray(allOrders) || allOrders.length === 0) {
    return <p>No orders to display.</p>;
  }

  return (
    <>
      <Row className="p-5">
        <Col xs={6} className="text-start">
          <h1>Admin Dashboard</h1>
        </Col>
        <Col xs={6} className="text-end">
          <Button
            variant="none"
            style={{ color: 'black', backgroundColor: '#DCD9B9' }}
            onClick={handleAddProduct}
          >
            + Add Product
          </Button>
        </Col>
      </Row>

      <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
        <Table bordered hover style={{ width: '90%' }} size="lg">
          <thead>
            <tr>
              <th style={{ verticalAlign: 'middle', textAlign: 'center' }}>USER ID</th>
              <th style={{ verticalAlign: 'middle', textAlign: 'center' }}>ORDER ID</th>
              <th style={{ verticalAlign: 'middle', textAlign: 'center' }}>DATE OF PURCHASE</th>
              <th style={{ verticalAlign: 'middle', textAlign: 'center', width: '25%' }}>PRODUCTS</th>
              <th style={{ verticalAlign: 'middle', textAlign: 'center' }}>TOTAL AMOUNT</th>
            </tr>
          </thead>

          <tbody>
            {allOrders.map((order) => (
              <tr key={order._id}>
                <td>{order.userId}</td>
                <td>{order._id}</td>
                <td>{order.purchasedOn}</td>
                <td style={{ maxWidth: '25%' }}>
                  <ul>
                    {order.products.map((product) => (
                      <li key={product.productId}>
                        {product.productId} - {product.quantity}
                      </li>
                    ))}
                  </ul>
                </td>
                <td>₱ {order.totalAmount}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
    </>
  );
}

OrdersTable.propTypes = {
  allOrders: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string.isRequired,
      userId: PropTypes.string.isRequired,
      purchasedOn: PropTypes.string.isRequired,
      products: PropTypes.arrayOf(
        PropTypes.shape({
          productId: PropTypes.string.isRequired,
          quantity: PropTypes.number.isRequired,
        })
      ).isRequired,
      totalAmount: PropTypes.number.isRequired,
    })
  ).isRequired,
};
