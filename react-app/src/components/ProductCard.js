import { Card, Button, Col, Row} from 'react-bootstrap';
import { useNavigate,Link } from 'react-router-dom';
import PropTypes from 'prop-types';

export default function ProductCard({ product }) {
  const { _id, name, description, price } = product;

  const navigate = useNavigate;

 return (
    <Row className="p-1 my-auto" style={{backgroundColor: '#DCD9B9' }}>
      <Col xs={12} className="text-center mb-4">
        <Row className="justify-content-center">
          <Col xs={12} md={8} lg={6}>
            <Card className="p-3 mx-auto">
              <Card.Body>
                <Card.Title style={{fontSize: '1.5rem', fontWeight: 'bold'}}>{name}</Card.Title>
                <br/>
                <Card.Subtitle style={{fontSize: '1.35rem'}}>Description:</Card.Subtitle>
                <Card.Text className="mx-5" style={{fontSize: '1.1rem'}}>{description}</Card.Text>

                <Row>
                  <Col className="d-flex justify-content-center">
                    <Card.Subtitle style={{ fontSize: '1.35rem' }}>Price:</Card.Subtitle>
                    <Card.Text style={{fontSize: '1.25rem', marginLeft: '0.5rem'}} className="my-auto">₱ {price}</Card.Text>
                  </Col>
                </Row>
                <br/>
                <Link
                  className="btn"
                  variant="dark"
                  style={{ color: 'black', backgroundColor: '#DCD9B9' }}
                  to={`/products/active/${_id}/view`}
                >
                  View Products
                </Link>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Col>
    </Row>
  );
}


ProductCard.propTypes = {
  product: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
  }),
};