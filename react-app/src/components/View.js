import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function View() {
  const navigate = useNavigate();
  const { user } = useContext(UserContext);
  const { userId, productId } = useParams();
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [originalPrice, setOriginalPrice] = useState(0);
  const [quantity, setQuantity] = useState(1);

  const quantityChange = (event) => {
    setQuantity(event.target.value);
  };

  const calculateTotalPrice = () => {
    return originalPrice * quantity;
  };

  const checkout = () => {
    const totalPrice = calculateTotalPrice();

    fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        userId: user.id,
        products: [
          {
            productId: productId,
            quantity: quantity,
          },
        ],
        totalAmount: totalPrice,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.message) {
          Swal.fire({
            title: 'Success',
            icon: 'success',
            text: result.message,
          });

          navigate('/products');
        }
      })
      .catch((error) => {
        Swal.fire({
          title: 'Oops',
          icon: 'error',
          text: `Something went wrong :( ${error.message}`,
        });
      });
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((response) => response.json())
      .then((result) => {
        setName(result.name);
        setDescription(result.description);
        setOriginalPrice(parseFloat(result.price));
      });
  }, [productId]);

  return (
    <Container className="mt-5">
      <Row>
        <Col xs={12} md={4} className="mx-auto">
          <Card className="cardView p-3 mx-auto" style={{backgroundColor: '#DCD9B9'}}>
            <Card.Body>
              <Card.Title>{name}</Card.Title>

              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>

              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>₱ {originalPrice.toString()}</Card.Text>

              <Form.Group controlId="quantity">
                <Form.Label>Quantity:</Form.Label>
                <Form.Control
                  type="number"
                  min={1}
                  value={quantity}
                  onChange={quantityChange}
                />
              </Form.Group>

              <br />

              <Card.Subtitle>Total Price:</Card.Subtitle>
              <Card.Text>₱ {calculateTotalPrice()}</Card.Text>

              {user.id !== null ? (
                <Button variant="dark" style={{ color: 'black', backgroundColor: 'white'}} onClick={checkout}>
                  Checkout
                </Button>
              ) : (
                <Link className="btn" variant="dark" style={{ color: 'black', backgroundColor: 'white'}} to="/login">
                  Login to Order
                </Link>
              )}
            </Card.Body>
          </Card>
          <br/>
          <Row className="justify-content-center mx-auto">
          <Button variant="dark" style={{ color: 'black', backgroundColor: '#DCD9B9'}} as={Link} to="" onClick={() => navigate(-1)}>
           Go back
          </Button>
          </Row>
        </Col>
      </Row>
    </Container>
  );
}