import { useState, useEffect } from 'react';
import { Container, Card, Button, Form } from 'react-bootstrap';
import { useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function ViewAdd() {
  const navigate = useNavigate();

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const [isActive, setIsActive] = useState(false);

  const createProduct = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/add`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.message) {
          // Clear the form
          setName('');
          setDescription('');
          setPrice('');

          Swal.fire({
            title: 'Success',
            icon: 'success',
            text: result.message,
          });
          navigate('/dashboard');
        }
      })
      .catch((error) => {
        Swal.fire({
          title: 'Oops',
          icon: 'error',
          text: `Something went wrong :( ${error.message}`,
        });
      });
  };

  useEffect(() => {
    if (name !== '' && description !== '' && price !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [name, description, price]);

  return (
    <Container className="d-flex justify-content-center align-items-center pt-5">
      <Card style={{ width: '22rem', height: '26rem', backgroundColor: '#DCD9B9'}}>
        <Card.Body>
          <div className="d-flex justify-content-center">
            <Card.Title style={{ fontSize: '2rem', fontWeight: 'bold' }}>Add New Product</Card.Title>
          </div>

          <br />

          <Form onSubmit={(event) => event.preventDefault()}>
            <Form.Group controlId="name">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Product Name"
                value={name}
                onChange={(event) => setName(event.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="description">
            <Form.Label>Description</Form.Label>
            <Form.Control
              as="textarea"
              placeholder="Enter Product Description"
              value={description}
              onChange={(event) => setDescription(event.target.value)}
              required
            />
          </Form.Group>


            <Form.Group controlId="price">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                value={price}
                onChange={(event) => setPrice(event.target.value)}
                required
              />
            </Form.Group>

            <br/>

            <div className="d-flex justify-content-end">
              <Button variant="dark" style={{ color: 'black', backgroundColor: 'white'}} type="submit" disabled={!isActive} onClick={createProduct}>
                Submit
              </Button>

              <div style={{width: '5px'}}></div> 
              <Button variant="dark" style={{color: 'black', backgroundColor: 'white'}} as={Link} to="/dashboard">
                Close
              </Button>
            </div>
          </Form>
        </Card.Body>
      </Card>
    </Container>
  );
}