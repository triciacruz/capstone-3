import React, { useState, useEffect } from 'react';
import { Container, Card, Button } from 'react-bootstrap';
import { useNavigate, Link, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function ViewArchive() {
  const navigate = useNavigate();
  const { productId } = useParams();

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');

  useEffect(() => {
    fetchProductDetails();
  }, []);

  const fetchProductDetails = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((response) => response.json())
      .then((data) => {
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
      })
      .catch((error) => {
        Swal.fire({
          title: 'Oops',
          icon: 'error',
          text: `Something went wrong :( ${error.message}`,
        });
      });
  };

  const archiveProduct = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.message) {
          Swal.fire({
            title: 'Success',
            icon: 'success',
            text: 'Product archived successfully',
          });
          navigate('/dashboard');
        }
      })
      .catch((error) => {
        Swal.fire({
          title: 'Oops',
          icon: 'error',
          text: `Something went wrong :( ${error.message}`,
        });
      });
  };

  return (
    <Container className="d-flex justify-content-center align-items-center pt-5">
      <Card style={{ width: '22rem', height: '23rem', backgroundColor: '#DCD9B9'}}>
        <Card.Body>
          <div className="d-flex justify-content-center">
            <Card.Title style={{ fontSize: '2rem', fontWeight: 'bold'}}>Archive Product</Card.Title>
          </div>

          <br />

          <Card.Title>{name}</Card.Title>
           <br />
          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{description}</Card.Text>
          
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>₱ {price}</Card.Text>

          <div className="d-flex justify-content-end">
            <Button
              variant="dark"
              style={{ color: 'black', backgroundColor: 'white' }}
              onClick={archiveProduct}
            >
              Disable
            </Button>

            <div style={{ width: '5px' }}></div>
            <Button
              variant="dark"
              style={{ color: 'black', backgroundColor: 'white' }}
              as={Link}
              to="/dashboard"
            >
              Close
            </Button>
          </div>
        </Card.Body>
      </Card>
    </Container>
  );
}