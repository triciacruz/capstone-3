import { Row, Col, Button, Carousel } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import bookishImage1 from '../img/bookish1.png';
import bookishImage2 from '../img/bookish2.png';
import bookishImage3 from '../img/bookish3.png';

export default function About() {
  return (
    <Row className="justify-content-center align-items-center">
      <Col xs={12} className="text-center pt-4">
        <h1 style={{ color: '#DCD9B9', fontSize: '4rem' }}>About Us</h1>
      </Col>

      <Col xs={12} className="py-3">
        <Carousel style={{ marginBottom: '2rem', width: '85%',margin: '0 auto'}}>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src={bookishImage1}
              alt="Bookish1"
            />
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src={bookishImage2}
              alt="Bookish2"
            />
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src={bookishImage3}
              alt="Bookish3"
            />
          </Carousel.Item>
        </Carousel>
      </Col>

       <Col xs={12} className="text-center" >
        <div style={{ marginBottom: '2rem', width: '85%', margin: '0 auto'}}>
          <p style={{ fontSize: '1.2rem', lineHeight: '1.5', textAlign: 'justify' }}>
            Welcome to The Bookish Hub, your ultimate online destination for all things book-related! We are an ecommerce bookstore committed to providing book enthusiasts with a seamless and immersive shopping experience. At The Bookish Hub, we understand the power of literature to inspire, educate, and entertain, and we aim to bring the joy of reading right to your fingertips.
            <br /><br />
            Our carefully curated collection features a diverse range of genres, from beloved classics to contemporary bestsellers, ensuring there's something for every reader. Whether you're seeking an escape into a fictional world, expanding your knowledge with insightful non-fiction, or introducing your little ones to the wonders of storytelling, The Bookish Hub has you covered.
            <br /><br />
            In addition to our extensive book selection, we strive to foster a vibrant community of bibliophiles. Join us for virtual book clubs, author interviews, and engaging discussions, as we celebrate the magic of words together. With our user-friendly interface and reliable customer service, we aim to make your book-buying experience enjoyable and convenient. Let The Bookish Hub be your trusted companion on your literary journey. Happy reading!
          </p>
        </div>
      </Col>
    </Row>
  );
}