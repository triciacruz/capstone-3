import { useState, useEffect } from 'react';
import DashboardTable from '../components/DashboardTable';
import LoadingSpinner from '../components/LoadingSpinner';
import { Row, Col } from 'react-bootstrap';

export default function Dashboard() {
  const [allProducts, setAllProducts] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);

    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
      .then(response => response.json())
      .then(result => {
        setIsLoading(false);
        setAllProducts(result);
      })
      .catch(error => {
        setIsLoading(false);
        console.error('Error fetching data:', error);
      });
  }, []);

  return (
    <Row>
      <Col>
        {isLoading ? (
          <LoadingSpinner />
        ) : (
          <DashboardTable allProducts={allProducts} />
        )}
      </Col>
    </Row>
  );
}