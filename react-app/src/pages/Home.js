import { Fragment, useContext } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import UserContext from '../UserContext';
import backgroundImage from '../img/bookish.png';

export default function Home() {
  const { user } = useContext(UserContext);

  let isAdmin = false;
  if (user && user.isAdmin) {
    isAdmin = true;
  }

  return (
    <Fragment>
      {user ? (
        <Banner isAdmin={isAdmin} />
      ) : (
        <Banner isAdmin={false} />
      )}
      <Highlights/>
    </Fragment>
  );
}
