import { useState, useEffect, useContext } from 'react';
import { Form, Button, Card, Container } from 'react-bootstrap';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Login() {
  const { user, setUser } = useContext(UserContext);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(false);

  function loginUser(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (typeof result.accessToken !== 'undefined') {
          localStorage.setItem('token', result.accessToken);

          retrieveUserDetails(result.accessToken);

          setEmail('');
          setPassword('');

          Swal.fire({
            title: 'Login Successful',
            icon: 'success',
            text: 'Welcome!',
          });
        }
      })
      .catch((error) => {
        Swal.fire({
          title: 'Authentication failed',
          icon: 'error',
          text: 'Kindly check your login credentials.',
        });
      });
  }

  // Stores the user details from the token to the global user state
  const retrieveUserDetails = (token) => {
    fetch('http://localhost:3000/users/details', {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        setUser({
          /* global user state data (app.js) */
          /* aside from email, these two are added */
          id: result._id,
          isAdmin: result.isAdmin,
        });
      });
  }

  useEffect(() => {
    if (email !== '' && password !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return (
    <>
      {user.id !== null && user.isAdmin ? (
        <Navigate to="/dashboard" />
      ) : user.id !== null && !user.isAdmin ? (
        <Navigate to="/" />
      ) : (
        <Container className="d-flex justify-content-center align-items-center pt-5">
          <Card style={{ width: '22rem', height: '25rem', backgroundColor: '#DCD9B9'}}>
            <Card.Body>
             <div className="d-flex justify-content-center">
              <Card.Title style={{ fontSize: '3rem', fontWeight: 'bold'}}>Login</Card.Title>
              </div>
              
              <br/>
              
              <Form onSubmit={(event) => loginUser(event)}>
                <Form.Group controlId="userEmail">
                  <Form.Label>Email address</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="Enter email"
                    value={email}
                    onChange={(event) => setEmail(event.target.value)}
                    required
                  />
                  <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                  </Form.Text>
                </Form.Group>

                <Form.Group controlId="password">
                  <Form.Label className="pt-3">Password</Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Enter password"
                    value={password}
                    onChange={(event) => setPassword(event.target.value)}
                    required
                  />
                </Form.Group>

                <br />

                <div className="d-flex justify-content-center">
                  {isActive ? (
                    <Button variant="dark" style={{color: 'black', backgroundColor: 'white'}} type="submit">
                      Submit
                    </Button>
                  ) : (
                    <Button disabled variant="dark" style={{color: 'black', backgroundColor: 'white'}} type="submit">
                      Submit
                    </Button>
                  )}
                </div>
              </Form>
            </Card.Body>
          </Card>
        </Container>
      )}
    </>
  );
}
