import {Link, useNavigate} from 'react-router-dom'
import {Fragment} from 'react'
import {Button} from 'react-bootstrap'

export default function Notfound(){
	
	const navigate = useNavigate()

	return(
		<Fragment>
			<h1>Sorry... Page Not Found</h1>
			<p>The page you are looking for cannot be found.</p>
			{/*<Link as={Link} to="" onClick={() => navigate(-1)}>Go back</Link>*/}
			<Button variant="dark" style={{color: 'black', backgroundColor: '#DCD9B9'}} as={Link} to="" onClick={() => navigate(-1)}>Go back</Button>
		</Fragment>
	)
}