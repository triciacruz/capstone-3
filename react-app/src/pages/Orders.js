import { useState, useEffect } from 'react';
import OrdersTable from '../components/OrdersTable';
import LoadingSpinner from '../components/LoadingSpinner';
import { Row, Col } from 'react-bootstrap';

export default function Orders() {
  const [allOrders, setAllOrders] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    setIsLoading(true);

    fetchOrdersFromDatabase()
      .then(result => {
        setIsLoading(false);
        setAllOrders(result);
      })
      .catch(error => {
        setIsLoading(false);
        setError(error.message);
        console.error('Error fetching data:', error);
      });
  }, []);

  // Fetch orders from the database
  const fetchOrdersFromDatabase = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/orders/all`);
      if (!response.ok) {
        throw new Error('Failed to fetch orders from the database');
      }
      const data = await response.json();
      return data;
    } catch (error) {
      throw new Error(error.message);
    }
  };

  return (
    <Row>
      <Col>
        {isLoading ? (
          <LoadingSpinner />
        ) : error ? (
          <p>Error: {error}</p>
        ) : (
          <OrdersTable allOrders={allOrders} />
        )}
      </Col>
    </Row>
  );
}
