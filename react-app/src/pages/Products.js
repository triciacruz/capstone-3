import {useState, useEffect, Fragment} from 'react'
import ProductCard from '../components/ProductCard'
import LoadingSpinner from '../components/LoadingSpinner'
import { Row, Col, Container} from 'react-bootstrap'

export default function Products(){
	const [products, setProducts] = useState([])
	const [isLoading, setIsLoading] = useState(false)

	useEffect(() => {
		setIsLoading(true)

		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(response => response.json())
		.then(result => {
			setIsLoading(false)

			setProducts(result.map(product => {
				return (
					<ProductCard key={product._id} product={product}/>
				)
			}))
		})
	}, [])

	return(
		<Row>
			<Col>
				{ isLoading ?
						<LoadingSpinner/>
					:
				   products 
				}
			</Col>
		</Row>
	)
}