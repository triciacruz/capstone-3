import { useState, useEffect, useContext } from 'react';
import { Form, Button, Card, Container, Row, Col } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register() {
  const navigate = useNavigate();

  const { user } = useContext(UserContext);

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobileNumber, setMobileNumber] = useState('');
  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const [isActive, setIsActive] = useState(false);

  function registerUser(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/check-email`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result === true) {
          Swal.fire({
            title: 'User Already exists',
            icon: 'error',
            text: 'The email you provided already exists on the server! Provide another email for registration.',
          });
        } else {
          fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
              mobileNumber: mobileNumber,
              password: password1,
            }),
          })
            .then((response) => response.json())
            .then((result) => {
              if (typeof result !== 'undefined') {
                // For clearing out the form
                setFirstName('');
                setLastName('');
                setMobileNumber('');
                setEmail('');
                setPassword1('');
                setPassword2('');

                Swal.fire({
                  title: 'Registration Successful',
                  icon: 'success',
                  text: 'Welcome!',
                });

                navigate('/login');
              }
            })
            .catch((error) => {
              Swal.fire({
                title: 'Something went wrong',
                icon: 'error',
                text: error.message,
              });
            });
        }
      });
  }

  useEffect(() => {
    if (
      firstName !== '' &&
      lastName !== '' &&
      mobileNumber !== '' &&
      email !== '' &&
      password1 !== '' &&
      password2 !== '' &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, mobileNumber, email, password1, password2]);

  return (
    <>
      {user.id !== null ? (
        <Navigate to="/" />
      ) : (
        <Container className="d-flex justify-content-center align-items-center pt-5">
          <Card style={{ width: '22rem', height: '31rem', backgroundColor: '#DCD9B9'}}>
            <Card.Body>
              <div className="d-flex justify-content-center">
                <Card.Title style={{ fontSize: '3rem', fontWeight: 'bold'}}>Register</Card.Title>
              </div>

              <br />

              <Form onSubmit={(event) => registerUser(event)}>
                <Row>
                  <Col>
                    <Form.Group controlId="firstName">
                      <Form.Label>First Name</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="Enter first name"
                        value={firstName}
                        onChange={(event) => setFirstName(event.target.value)}
                        required
                      />
                    </Form.Group>
                  </Col>
                  <Col>
                    <Form.Group controlId="lastName">
                      <Form.Label>Last Name</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="Enter last name"
                        value={lastName}
                        onChange={(event) => setLastName(event.target.value)}
                        required
                      />
                    </Form.Group>
                  </Col>
                </Row>

                <Form.Group controlId="userEmail">
                  <Form.Label>Email address</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="Enter email"
                    value={email}
                    onChange={(event) => setEmail(event.target.value)}
                    required
                  />
                  <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                  </Form.Text>
                </Form.Group>

                <Form.Group controlId="userMobileNo">
                  <Form.Label>Mobile Number</Form.Label>
                  <Form.Control
                    type="mobile"
                    placeholder="Enter mobile number"
                    value={mobileNumber}
                    onChange={(event) => setMobileNumber(event.target.value)}
                    required
                  />
                </Form.Group>

                <Row>
                  <Col>
                    <Form.Group controlId="password1">
                      <Form.Label>Password</Form.Label>
                      <Form.Control
                        type="password"
                        placeholder="Enter password"
                        value={password1}
                        onChange={(event) => setPassword1(event.target.value)}
                        required
                      />
                    </Form.Group>
                  </Col>
                  <Col>
                    <Form.Group controlId="password2">
                      <Form.Label>Verify Password</Form.Label>
                      <Form.Control
                        type="password"
                        placeholder="Enter password"
                        value={password2}
                        onChange={(event) => setPassword2(event.target.value)}
                        required
                      />
                    </Form.Group>
                  </Col>
                </Row>

                <br />

                <div className="d-flex justify-content-center">
                  {isActive ? (
                    <Button
                      variant="dark"
                      style={{ color: 'black', backgroundColor: 'white' }}
                      type="submit"
                    >
                      Submit
                    </Button>
                  ) : (
                    <Button
                      disabled
                      variant="dark"
                      style={{ color: 'black', backgroundColor: 'white' }}
                      type="submit"
                    >
                      Submit
                    </Button>
                  )}
                </div>
              </Form>
            </Card.Body>
          </Card>
        </Container>
      )}
    </>
  );
}